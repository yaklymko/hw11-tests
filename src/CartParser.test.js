import CartParser from './CartParser';
import path from "path";

let parser;
const cart = {
    "items": [
        {
            "id": "3e6def17-5e87-4f27-b6b8-ae78948523a9",
            "name": "Mollis consequat",
            "price": 9,
            "quantity": 2
        },
        {
            "id": "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
            "name": "Tvoluptatem",
            "price": 10.32,
            "quantity": 1
        },
        {
            "id": "33c14844-8cae-4acd-91ed-6209a6c0bc31",
            "name": "Scelerisque lacinia",
            "price": 18.9,
            "quantity": 1
        },
        {
            "id": "f089a251-a563-46ef-b27b-5c9f6dd0afd3",
            "name": "Consectetur adipiscing",
            "price": 28.72,
            "quantity": 10
        },
        {
            "id": "0d1cbe5e-3de6-4f6a-9c53-bab32c168fbf",
            "name": "Condimentum aliquet",
            "price": 13.9,
            "quantity": 1
        }
    ],
    "total": 348.32
};

beforeEach(() => {
    parser = new CartParser();
});

describe('CartParser - unit tests', () => {

    test("should calc total", () => {
        expect(parser.calcTotal(cart.items)).toBeCloseTo(cart.total);
    });

    test("should validate successful", () => {
        let testContent = 'Product name,Price,Quantity';
        let errorsArr = parser.validate(testContent);
        expect(errorsArr).toEqual([]);
    });

    test("should validate error", () => {
        const testContent = 'Productname,PriceQuantity';
        const errorsArr = parser.validate(testContent);
        expect(errorsArr.length).not.toBe(0);
    });

    test("should validate throw column and row", () => {
        let testContent = 'Productname,Price,Quantity';
        let errorsArr = parser.validate(testContent);
        expect(errorsArr[0]).toEqual(expect.objectContaining({column: 0, row: 0}));

        testContent = 'Product name,Price,Quantity' + "\nMollisconsequat,-9.00,2";
        errorsArr = parser.validate(testContent);
        expect(errorsArr[0]).toEqual(expect.objectContaining({column: 1, row: 1}));

        testContent = 'Product name,Price,Quantity' + "\n,-9.00,2";
        errorsArr = parser.validate(testContent);
        expect(errorsArr[0]).toEqual(expect.objectContaining({column: 0, row: 1}));

        testContent = 'Product name,Price,Quantity' + "\n,,2";
        errorsArr = parser.validate(testContent);
        expect(errorsArr[0]).toEqual(expect.objectContaining({column: 0, row: 1}));
    });

    test("should validate check field types", () => {
        let testContent = 'Product name,Price,Quantity' + "\n,9.00,2";
        let errorsArr = parser.validate(testContent);
        expect(errorsArr.length).not.toBe(0);

        testContent = 'Product name,Price,Quantity' + "\nname,-1,2";
        errorsArr = parser.validate(testContent);
        expect(errorsArr.length).not.toBe(0);

        testContent = 'Product name,Price,Quantity' + "\nname,1,- 2";
        errorsArr = parser.validate(testContent);
        expect(errorsArr.length).not.toBe(0);

    });

    test("should validate throw error types", () => {
        let testContent = 'Productname,Price,Quantity';
        let errorsArr = parser.validate(testContent);
        expect(errorsArr[0]).toEqual(expect.objectContaining({type: "header"}));

        testContent = 'Product name,Price,Quantity' + '\n,9.00,2';
        errorsArr = parser.validate(testContent);
        expect(errorsArr[0]).toEqual(expect.objectContaining({type: "cell"}));

        testContent = 'Product name,Price,Quantity' + '\n9.00,2';
        errorsArr = parser.validate(testContent);
        expect(errorsArr[0]).toEqual(expect.objectContaining({type: "row"}));
    });

    test("should parse line successful", () => {
        const testContent = 'Product name, 15, 2';
        expect(parser.parseLine(testContent)).toEqual(expect.objectContaining({
            name: 'Product name',
            price: 15,
            quantity: 2
        }));
    });

    test("should parse line add id ", () => {
        const testContent = 'Product name, 15, 2';
        expect(parser.parseLine(testContent)).not.toBeUndefined();
    });


});

describe('CartParser - integration test', () => {

    test("should parse success", () => {
        const filePath = path.join(__dirname, "../samples/cart.csv");
        const res = parser.parse(filePath);

        expect(res.items[0]).toEqual(expect.objectContaining({
            name: 'Mollis consequat',
            price: 9,
            quantity: 2,
        }));

        expect(res.items[res.items.length - 1]).toEqual(expect.objectContaining({
                name: 'Condimentum aliquet',
                price: 13.9,
                quantity: 1,
            }
        ));


    });
});